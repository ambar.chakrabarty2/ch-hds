#Azure Data Factory (ADF) Git Repo
This repository stores ADF resources:
- Pipelines
- Datasets
- Linked Services
- Integration Runtimes
- Triggers

The work contained here is source-code. The `master` branch contains code that has been peer reviewed. 

#Deployment of ADF
When changes are ready to be rolled out across environments and into Production, ADF auto-generates an ARM template and pushes it into the `adf_publish` branch. This branch represents 'what is ready to ship'. It will be brought into the deployment repo in Gitlab to serve as a resource during the environment deployment pipeline. 
