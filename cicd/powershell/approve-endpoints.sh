#!/bin/bash

set -e

# Get the directory that this script is in so the script will work regardless
# of where the user calls it from. If the scripts or its targets are moved,
# these relative paths will need to be updated.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export DIR

#
# Argument parsing
#

function show_usage() {
    echo "approve-endpoints.sh"
    echo
    echo -e "\t--storage-id\tThe Azure resource ID of the storage account'"
    echo -e "\t--sql-id\tThe Azure resource ID of the synapse server'"
}


STORAGE_ID=""
SQL_ID=""
while [[ $# -gt 0 ]]
do
    case "$1" in 
        --storage-id)
            STORAGE_ID=$2
            shift 2
            ;;
         --sql-id)
            SQL_ID=$2
            shift 2
            ;;
        *)
            echo "Unexpected '$1'"
            show_usage
            exit 1
            ;;
    esac
done

echo "variable ouput"
echo $STORAGE_ID
echo $SQL_ID

# Get all the pending private endpoints for this storage account and approve them. This method should be used for other resources.
echo -e "\n>>> Checking if an endpoint needs approving for storage:"
endpoint_ids=$(az network private-endpoint-connection list --id $"STORAGE_ID" --query "[?properties.privateLinkServiceConnectionState.status=='Pending'].id" --output tsv) 
for id in $endpoint_ids
do
    echo "$id will be approved..." 
    az network private-endpoint-connection approve --id "$id" --description "Auto-approved during build"
done

# Get all the pending private endpoints for this SQL Serverand approve them. This method should be used for other resources.
echo -e "\n>>> Checking for SQL private endpoints to approve:"
endpoint_ids=$(az network private-endpoint-connection list --id "$SQL_ID" --query "[?properties.privateLinkServiceConnectionState.status=='Pending'].id" --output tsv) 
for sql_id in $sql_endpoint_ids
do
    echo "$sql_id will be approved..." 
    az network private-endpoint-connection approve --id "$sql_id" --description "Auto-approved during build"
done