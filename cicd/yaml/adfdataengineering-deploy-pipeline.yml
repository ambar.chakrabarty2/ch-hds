###################################
# Define parameters & jobs
###################################
parameters:
  - name: env
    type: string
    values:
    - devt
    - test
    - stag
    - prod
  - name: ServiceConnectionName
    type: string
  - name: DataFactoryName
    type: string
  - name: DataFactoryResourceGroupName
    type: string
 
jobs:

  - job: GetSecretsFromKV_${{ parameters.env }}
    pool: 'ch-hds-${{ parameters.env }}-pool'
    steps:
    - task: AzureKeyVault@2
      inputs:
        azureSubscription: ${{ parameters.ServiceConnectionName }}
        KeyVaultName: 'kv-uks-${{ parameters.env }}-ch-hds-core'
        SecretsFilter: 'databricksWorkspaceUrl,databricksClusterId'
        RunAsPreJob: false
    - powershell: |
        Write-Host "##vso[task.setvariable variable=output_databricksWorkspaceUrl;issecret=false;isOutput=true]$(databricksWorkspaceUrl)"
        Write-Host "##vso[task.setvariable variable=output_databricksClusterId;issecret=false;isOutput=true]$(databricksClusterId)"
      name: GetSecretsFromKV


  - deployment: deploymentjob${{ parameters.env }}
    dependsOn: GetSecretsFromKV_${{ parameters.env }}
    variables: 
    - name: bricksUrl
      value: $[ dependencies.GetSecretsFromKV_${{ parameters.env }}.outputs['GetSecretsFromKV.output_databricksWorkspaceUrl'] ]
    - name: bricksClusterId
      value: $[ dependencies.GetSecretsFromKV_${{ parameters.env }}.outputs['GetSecretsFromKV.output_databricksClusterId'] ]
    displayName: Deployment Job ${{ parameters.env }} 
    environment: deploy ${{ parameters.env }}
    strategy:
      runOnce:
        deploy:
          steps:
          - checkout: self
            displayName: '1 Retrieve Repository'
            clean: true 
      
###################################
# 0 Change runtime status in template 
###################################
          - powershell: |
              $ARMTemplateToManage = Get-ChildItem -Path $(Pipeline.Workspace)\ArmTemplatesArtifact\ARMTemplateForFactory.json -Recurse -Force
              foreach ($MyFile in $ARMTemplateToManage)
              {
                  Write-Host "JSON File to change -", $MyFile                  
                  (Get-Content $MyFile -Encoding UTF8) -replace "`"runtimeState`":` `"Stopped`"", '$(runtimeStatus)' | Set-Content $MyFile
                  Write-Host "JSON File changed -", $MyFile 
                  Write-Host " -----------------------------------"
              }  
            displayName: '0 Change trigger runtime status in template'            
 
###################################
# 1 Show environment and treeview
###################################
          - powershell: |
              Write-Output "This is the ${{ parameters.env }} environment"
              tree "$(Pipeline.Workspace)" /F
            displayName: '1 Show environment and treeview Pipeline_Workspace'
             
###################################
# 2 Stop triggers
###################################
          - task: AzurePowerShell@5
            displayName: '2 Stop triggers'
            inputs:
              azureSubscription: ${{ parameters.ServiceConnectionName }}
              pwsh: true
              azurePowerShellVersion: LatestVersion
              scriptType: filePath
              scriptPath: '$(Pipeline.Workspace)\ArmTemplatesArtifact\PrePostDeploymentScript.ps1'
              scriptArguments: > 
                -armTemplate '$(Pipeline.Workspace)\ArmTemplatesArtifact\ARMTemplateForFactory.json'
                -ResourceGroupName ${{ parameters.DataFactoryResourceGroupName }}
                -DataFactoryName ${{ parameters.DataFactoryName }}
                -predeployment $true
                -deleteDeployment $false

###################################
# 3 Remove resource group lock
###################################
          - task: AzureCLI@2
            displayName: "3 Remove resource group lock"
            inputs:
              azureSubscription: ${{ parameters.ServiceConnectionName }}
              scriptType: bash
              scriptLocation: 'inlineScript'
              inlineScript: 'az group lock delete -n ${{ parameters.DataFactoryResourceGroupName }}-Lock-DoNotDelete -g ${{ parameters.DataFactoryResourceGroupName }}'

###################################
# 4 Deploy ADF Artifact
###################################
          - task: AzureResourceManagerTemplateDeployment@3
            displayName: '4 Deploy ADF Artifact'
            inputs:
              deploymentScope: 'Resource Group'
              azureResourceManagerConnection: ${{ parameters.ServiceConnectionName }}
              subscriptionId: $(subscription-id)
              action: 'Create Or Update Resource Group'
              resourceGroupName: ${{ parameters.DataFactoryResourceGroupName }}
              location: 'UK South'
              templateLocation: 'Linked artifact'
              csmFile: '$(Pipeline.Workspace)/ArmTemplatesArtifact/ARMTemplateForFactory.json'
              csmParametersFile: '$(Pipeline.Workspace)/ArmTemplatesArtifact/ARMTemplateParametersForFactory.json'
              overrideParameters: >-
                -factoryName "${{ parameters.DataFactoryName }}"
                -AzureDataLakeStorage_properties_typeProperties_url "$(azuredatalakeurl)"
                -AzureKeyVault_properties_typeProperties_baseUrl "$(azurekeyvaulturl)"
                -AzureSynapseAnalytics_properties_typeProperties_connectionString "$(synapse-connectionstring)"
                -AzureSynapseAnalytics_properties_typeProperties_password_secretName "$(dataEngineerKeyVaultSecret)"
                -LS_Databricks_Eng_Standard_Cluster_properties_typeProperties_domain "https://$(bricksUrl)"
                -LS_Databricks_Eng_Standard_Cluster_properties_typeProperties_workspaceResourceId "$(databricks-resourceid)"
                -ADLSPrivateEndpoint_name "${{ parameters.DataFactoryName }}/default/ADLSPrivateEndpoint"
                -ADLSPrivateEndpoint_properties_privateLinkResourceId "/subscriptions/$(subscription-id)/resourceGroups/${{ parameters.DataFactoryResourceGroupName }}/providers/Microsoft.Storage/storageAccounts/struks$(envcode)chhds$(sub-envcode)"
                -ADLSPrivateEndpoint_properties_fqdns ["struks$(envcode)chhds$(sub-envcode).dfs.core.windows.net"]
                -SynapsePrivateEndpoint_name "${{ parameters.DataFactoryName }}/default/SynapsePrivateEndpoint"
                -SynapsePrivateEndpoint_properties_privateLinkResourceId "/subscriptions/$(subscription-id)/resourceGroups/${{ parameters.DataFactoryResourceGroupName }}/providers/Microsoft.Sql/servers/synapse-uks-$(envcode)-ch-hds-$(sub-envcode)"
                -SynapsePrivateEndpoint_properties_fqdns ["synapse-uks-$(envcode)-ch-hds-$(sub-envcode).database.windows.net"]
              deploymentMode: 'Incremental'
 
            env: 
                SYSTEM_ACCESSTOKEN: $(System.AccessToken)
                 
###################################
# 5 Start triggers and cleanup
###################################
          - task: AzurePowerShell@5
            displayName: '5 Start triggers and cleanup'
            inputs:
              azureSubscription: ${{ parameters.ServiceConnectionName }}
              pwsh: true
              azurePowerShellVersion: LatestVersion
              scriptType: filePath
              scriptPath: '$(Pipeline.Workspace)\ArmTemplatesArtifact\PrePostDeploymentScript.ps1'
              scriptArguments: > 
                -armTemplate $(Pipeline.Workspace)/ArmTemplatesArtifact/ARMTemplateForFactory.json
                -ResourceGroupName ${{ parameters.DataFactoryResourceGroupName }}
                -DataFactoryName ${{ parameters.DataFactoryName }}
                -predeployment $false
                -deleteDeployment $false

###################################
# 6 Recreate resource group lock
###################################
          - task: AzureCLI@2
            displayName: "6 Recreate resource group lock"
            inputs:
              azureSubscription: ${{ parameters.ServiceConnectionName }}
              scriptType: bash
              scriptLocation: 'inlineScript'
              inlineScript: 'az group lock create --lock-type CanNotDelete -n ${{ parameters.DataFactoryResourceGroupName }}-Lock-DoNotDelete -g ${{ parameters.DataFactoryResourceGroupName }}'

###################################
# 7 Approve endpoints
###################################        
          - task: AzureCLI@2
            displayName: "7 Approve pending private-endpoints"
            inputs:
              azureSubscription: ${{ parameters.ServiceConnectionName }}
              scriptType: ps
              scriptLocation: 'inlineScript'
              inlineScript: |
                Write-Host "Approve pending storage private endpoints"
                $endpoint_ids = $(az network private-endpoint-connection list --id "$(storage-id)" --query "[?properties.privateLinkServiceConnectionState.status=='Pending'].id" --output tsv)
                foreach ($id in $endpoint_ids)
                {                        
                  Write-Host "$id will be approved..."
                  az network private-endpoint-connection approve --id "$id" --description "Auto-approved during build"
                }
                Write-Host "Approve pending synapse private endpoints"
                $endpoint_ids = $(az network private-endpoint-connection list --id "$(sql-id)" --query "[?properties.privateLinkServiceConnectionState.status=='Pending'].id" --output tsv)
                foreach ($id in $endpoint_ids)
                {                        
                  Write-Host "$id will be approved..."
                  az network private-endpoint-connection approve --id "$id" --description "Auto-approved during build"
                }